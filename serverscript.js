var http = require('http');
var fs = require('fs');


var server = http.createServer(function(req, res) {
    console.log('request was made: ' + req.url);
    if (req.url === '/home' || req.url == '/') {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        fs.createReadStream(__dirname + '/index.html').pipe(res);
    } else if (req.url === '/png') {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        fs.createReadStream(__dirname + '/png.html').pipe(res);
    } else if (req.url === '/solo') {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        fs.createReadStream(__dirname + '/solo.html').pipe(res);
    } else {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        fs.createReadStream(__dirname + '/index.html').pipe(res);
    }
});
server.listen(3000, '127.0.0.1');
console.log('server now listening at 3000');